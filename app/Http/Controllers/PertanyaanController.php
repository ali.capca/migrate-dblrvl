<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
USE DB;
//USE app\pertanyaanModel;
use App\pertanyaanModel;
class PertanyaanController extends Controller
{
    public function create (){
        return view ('modulPertanyaan.create');
    }

    public function store (Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);
        
        /* //Query Builder
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "tanggal_dibuat" => date('Y-m-d')
        ]);*/

       /* //Modelling ORM
        $hasil = new pertanyaanModel;
        $hasil->judul = $request["judul"];
        $hasil->isi = $request["isi"];
        $hasil->tanggal_dibuat = date('Y-m-d');
        $hasil-> save(); */
               //modelling Assignment 
        pertanyaanModel::create([
    		'judul' => $request->judul,
            'isi' => $request->isi,
            'tanggal_dibuat' => date('Y-m-d')
    	]);

        return redirect ('/pertanyaan')->with('berhasil','Berhasil diSimpan!');
    }
    public function index (){
        //query builder
        //$query = DB ::table('pertanyaan')->get();
       //ORM
        $query = pertanyaanModel::all();
        return view ('modulPertanyaan.viewPertanyaan',compact('query'));
    }
    public function show($idPertanyaan){
        //query buillder
        //$query = DB ::table('pertanyaan')->where('id',$idPertanyaan)->first();
        //dd($query);
        //orm
        $query = pertanyaanModel :: find($idPertanyaan);
        return view ('modulPertanyaan.detailPertanyaan',compact('query'));
    }
    public function edit($idPertanyaan){
        //query builder
       // $query = DB ::table('pertanyaan')->where('id',$idPertanyaan)->first();
       // orm 
       $query = pertanyaanModel :: find($idPertanyaan);
        //dd($query);
        return view ('modulPertanyaan.editPertanyaan',compact('query'));
    }
    public function update($idPertanyaan, Request $request ){
     //query builder
     /*
        $query = DB ::table('pertanyaan')
                    ->where('id',$idPertanyaan)
                    ->update([
                        "judul" => $request["judul"],
                        "isi" => $request["isi"],
                        "tanggal_diperbaharui" => date('Y-m-d')
                    ]); */

        //orm
        $query = pertanyaanModel :: where('id',$idPertanyaan)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "tanggal_diperbaharui" => date('Y-m-d')
        ]);
           
        //dd($query);
        return redirect ('/pertanyaan')->with('berhasil','Berhasil diUpdate!');
    }
    public function destroy($idPertanyaan){
       //query builder
        //$query = DB ::table('pertanyaan')->where('id',$idPertanyaan)->delete();
        
        //orm
        pertanyaanModel::destroy($idPertanyaan);    
        return redirect ('/pertanyaan')->with('berhasil','Berhasil diHapus!');
    }

}
