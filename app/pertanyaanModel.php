<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pertanyaanModel extends Model
{
    protected $table = "pertanyaan";
    protected $fillable = ["judul", "isi","tanggal_dibuat"];
    //protected $guarded = ["judul"];
}
