@extends('master')
@section('content')
<div class="col-md-6">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <!--<form role="form" action="/pertanyaan" method="post"> -->
              <form role="form" action="{{route('pertanyaan.store')}}" method="post">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul</label>
                    <input class="form-control" name="judul" value="{{old('judul','')}}">
                  </div>
                  @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                  <div class="form-group">
                  <label>Isi</label>
                        <textarea class="form-control" rows="3"  name="isi">
                        {{old('isi','')}}
                        </textarea>
                      </div>
                      @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                  </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
</div>
@endsection