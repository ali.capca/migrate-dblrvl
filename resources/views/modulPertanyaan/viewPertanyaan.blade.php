@extends('master')
@section('content')
@if (session('berhasil'))
<div class="alert alert-success">
{{session('berhasil')}}
</div>
@endif
<!--<a class= "btn btn-primary mb-2" href="/pertanyaan/create">Create</a>-->
<a class= "btn btn-primary mb-2" href="{{route('pertanyaan.create')}}">Create</a>
<table class="table table-bordered">
  <thead>                  
    <tr>
      <th style="width: 10px">No</th>
      <th>Judul</th>
      <th>Isi</th>
      <th style="width: 40px">Opsi</th>
    </tr>
  </thead>
  <tbody>
      @forelse($query as $key => $query )
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{$query -> judul}}</td>
      <td>{{$query -> isi}}</td>
      <td style="display: flex;">
      <!--<a class= "btn btn-primary btn-sm" href="/pertanyaan/{{$query -> id}}">Show</a>-->
      <a class= "btn btn-primary btn-sm" href="{{route('pertanyaan.show',['pertanyaan' => $query -> id])}}">Show</a>
      <!--<a class= "btn btn-default btn-sm" href="/pertanyaan/{{$query -> id}}/edit">edit</a>-->
      <a class= "btn btn-default btn-sm" href="{{route('pertanyaan.edit',['pertanyaan' => $query -> id],'edit')}}">edit</a>
      <!--<form action="/pertanyaan/{{$query -> id}}" method="post">
      @csrf
      @method('Delete')
      <input type="submit" class= "btn btn-danger btn-sm" value="Del">  </form>-->
      <form action="{{route('pertanyaan.destroy',['pertanyaan' => $query -> id])}}" method="post">
      @csrf
      @method('Delete')
      <input type="submit" class= "btn btn-danger btn-sm" value="Del">  </form>
      </td>
      
    </tr>
    @empty <p>No Data </p>
    @endforelse
    
  </tbody>
</table>
@endsection