<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

/*Route::get('master', function () {
    return view('master');
});
Route::get('items', function () {
    return view('items.index');
});
Route::get('data-table', function () {
    return view('items.dataTable');
});*/

/*Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@viewPertanyaan');
Route::get('/pertanyaan/{idPertanyaan}', 'PertanyaanController@detailPertanyaan');
Route::get('/pertanyaan/{idPertanyaan}/edit', 'PertanyaanController@editPertanyaan');
Route::put('/pertanyaan/{idPertanyaan}', 'PertanyaanController@updatePertanyaan');
Route::delete('/pertanyaan/{idPertanyaan}', 'PertanyaanController@deletePertanyaan'); */

Route :: resource('/pertanyaan', 'PertanyaanController');

